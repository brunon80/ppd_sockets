/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import static sockets.Client.*;

/**
 *
 * @author Bruno R
 */
public class ListenForSocket extends Thread{
 
    /**
     * Fluxo de execução dedicado a "ouvir" dados oriundos do servidor
     */
    @Override
        public void run () {
            //System.out.println("thread Cliente");
           
            try {
                soc = new Socket(host,1250);
                System.out.println("conectei");
                in = new DataInputStream(soc.getInputStream());
                out = new DataOutputStream(soc.getOutputStream());
                while (true) {                    
         
		   protocolRecieved = in.readUTF();
                   dataProtocolRecivied =  protocolRecieved.split("#");
                   player = dataProtocolRecivied[0];
                   origin = dataProtocolRecivied[1];

                   if (player.equals("Player2")){
                       if (firstConnection){
                           chatArea.append("<Moderador> Segundo jogador entrou! comece a jogar!\n");
                           chatArea.append("\n");
                            turn = true;
                            firstConnection = false;
                            myColorPieces =  blackIcon;
                            myMinPieces = blackMinIcon;
                            yourMinPieces = whiteMinIcon;
                            yourColorPieces = whiteIcon;
                       }
                       
                   }                    
                    else{
                         myColorPieces = whiteIcon;
                         myMinPieces = whiteMinIcon;
                         yourMinPieces = blackMinIcon;
                         yourColorPieces = blackIcon;
                    }
                   
                   if (origin.equals("trilha")){
                   
                       chatArea.append("<Moderador> " + player + " Fez trilha!!! Você vai perder uma peça :/ \n");
                       chatArea.append("\n");
                   }
                   if (origin.equals("piecetaked")){
                   
                       chatArea.append("<Moderador> " + player + " Tirou uma peça sua! \n");
                       chatArea.append("\n");
                   }
                   
                   if (origin.equals("restart")){
                       String answer = dataProtocolRecivied[2];
                       
                           if (answer.equals("yes")){
                                
                                GiveUpWindowForSure giveUpfSure = new GiveUpWindowForSure("O outro jogador deseja reiniciar a partida, você concorda?");
                                if (answer.equals(giveUpfSure.myAnswer)){
                                    ComunicationCtrl cm = new ComunicationCtrl();
                                    cm.restartResponse("yes");
                                    restart();
                                }
                           } 
                   }
                   
                   if (origin.equals("restartresponse")){
                    
                        String answer = dataProtocolRecivied[2];
                        if (answer.equals("yes")){
        
                            restart();
                            chatArea.append("<Moderador> O outro jogador concordou em reiniciar a partida!\n");
                            chatArea.append("\n");
                                
                           }
                   }
                   
                   if (origin.equals("giveup")){
                       String reason = dataProtocolRecivied[2];
                       AdvertiseWindow adv = new AdvertiseWindow(reason);
                       restart();
                       
                   }
                   
                   if (origin.equals("turn")){
                       msg = dataProtocolRecivied[2];
                       if(msg.equals("true")){
                       
                           turn = true;
                       }
                   }
                   
                   if (origin.equals("winner")){
                       JOptionPane.showMessageDialog(null, "Que pena, você perdeu! :/");
                       restart();
                   }
                   
                   if (origin.equals("message")){
                       msg = dataProtocolRecivied[2];
                       chatArea.append("<" + player + ">" + " "  + msg +"\n");
                       chatArea.append("\n");
                   }
                   if (origin.equals("game")){
                       //System.out.println("entrei em game");
                      nextRecivied = dataProtocolRecivied[2];
                     // System.out.println("passei 1");
                      lastRecivied = dataProtocolRecivied[3];
                     // System.out.println("passei 2");
                      nextIcon = new ImageIcon(dataProtocolRecivied[4]);
                      //System.out.println("passei 3");
                      //System.out.println(dataProtocolRecivied[5].toString());
                      lastIcon = new ImageIcon(dataProtocolRecivied[5]);
                      //System.out.println("passei 4");
                      
                      //System.out.println(msg);
                      //System.out.println(A0.getLocation().toString());
                        for (int i = 0; i< btnArray.length; i++){
                            if (btnArray[i].getLocation().toString().equals(nextRecivied)){ 
                                
                                btnArray[i].setIcon(nextIcon);                            
                            }
                          
                        }

                        for (int i = 0; i< btnArray.length; i++){
                            
                            if(btnArray[i].getLocation().toString().equals(lastRecivied)){

                                if (lastIcon == null) lastIcon = noIcon;
                                btnArray[i].setIcon(lastIcon);
                            }
                        }
                        
                   }

                }
                
                
            } catch (IOException e) {
            
                chatArea.append("<Moderador> Conexão falhou, tem certeza que o servidor está online? \n" + e.getMessage());
                chatArea.append("\n");
            }
        }
    }
