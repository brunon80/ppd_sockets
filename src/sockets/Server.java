package sockets;

import java.io.*;
import java.net.*;

public class Server{
		    
    static ServerSocket srvsoc = null;
    static String msg = "";
static class ServerSide extends Thread{

    @Override
    public void run() {
        try {
            srvsoc = new ServerSocket(1250);
            System.out.println("Aguardando Conexão" );
        } catch (IOException e) {
            System.out.println("Client connection error: " + e.getMessage());
        }
        while (true) {
            try {

                Socket s1 = srvsoc.accept();
                Socket s2 = srvsoc.accept();
                new Client(s1, s2,"Player1#").start();  // reads from s1 and redirects to s2                
                new Client(s2, s1,"Player2#").start();  // reads from s2 and redirects to s1
            } catch (IOException e) {
                System.out.println("Client connection error: " + e.getMessage());
            }
        }
    }
}



static class Client extends Thread {
    Socket s1;
    Socket s2;
    String player;

    Client(Socket s1, Socket s2, String player) {
        this.s1 = s1;
        this.s2 = s2;
        this.player = player;
    }

    public void run() {
        try {
            while (true) {                
                DataInputStream in = new DataInputStream(s1.getInputStream());
                DataOutputStream out = new DataOutputStream(s2.getOutputStream());
                
                out.writeUTF(player + in.readUTF());
            }
            
            
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }
    }  

    public static void main(String args[]){

            ServerSide server = new ServerSide();
            server.start();
    }
    

}


