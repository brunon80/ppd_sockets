/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;

import javax.swing.JOptionPane;

/**
 * Tela de confirmação de desistencia
 * @author Bruno R
 */
public class GiveUpWindowForSure {
    ComunicationCtrl cm = new ComunicationCtrl();
    String message;
    String myAnswer;

    /**
     * metodo que invoca a tela de confirmação de desistencia
     * @param message: menssagem a ser mostrada
     */
    public GiveUpWindowForSure(String message){
        
        
        int response = JOptionPane.showConfirmDialog(null, message, "Tem certeza?",
        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.NO_OPTION) {
            myAnswer = "no";
        } else if (response == JOptionPane.YES_OPTION) {
            myAnswer = "yes";
        } else if (response == JOptionPane.CLOSED_OPTION) {
          System.out.println("JOptionPane closed");
        }
  

    }
}
