/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import static sockets.Client.*;

/**
 *
 * @author Bruno R
 */
   public class ListenForToolBarButton implements ActionListener{
   
    /**
     * Classe que "escuta" eventos na Tollbar
     * @param e: origem do clique
     */
    @Override
        public void actionPerformed(ActionEvent e) {
        
            if (turn) {
               if(e.getSource() == newTurn){
                   connect.protocolMsgCtrl("passou a vez");
                    connect.protocolTurnCtrl();
                    turn = false;
                    if (nextPart == false) {
                        
                            lastButton = new JButton();
                        
                       
                        System.out.println("passei na primeira parte");

                            myPieces --;

                    }
                    if (nextPart == true) {

                        System.out.println("passei na segunda parte");
                        connect.protocolGameCtrl(tempbtn.getLocation(), lastButton.getLocation(), tempbtn.getIcon(), lastButton.getIcon());
                    }
                
                }
               if (e.getSource() == back){
                   rollBack();
               }
               
               
               if (e.getSource() == giveUp){
               
                   GiveUpWindow giveUp = new GiveUpWindow("Tem certeza que quer desistir?", "giveup");

               }
               
               if (e.getSource() == restart){
               
                   GiveUpWindow giveUp = new GiveUpWindow("Tem certeza que deseja Reiniciar?", "restart");
                   
               }
           }else{
                chatArea.append("<Moderador> Não é sua vez. Aguarde! \n");
                chatArea.append("\n");
               }
            if (e.getSource() == tutorial){
               
                   new HelpWindow();
               }
            
            if (e.getSource() == send){
                
                msgToSend = msgField.getText();
                try{
                       connect.protocolMsgCtrl(msgToSend);
                       chatArea.append("<Eu> " + msgToSend +"\n");
                       chatArea.append("\n");
                       msgField.setText("");

                    }catch(Exception ex){


                    }
            }
            
        }
        
        
       
   }
