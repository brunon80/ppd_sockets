/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;

import java.awt.Point;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import static sockets.Client.out;

/**
 * Essa classe possui todos os Protocolos de comunição necessarios para a aplicação funcionar dentro dos paramentros exigidos pelo professor
 * @author Bruno R
 */
public class ComunicationCtrl {
    
    /**
     * Protocolo de envio de menssagens
     * @param message: menssagem a ser enviada
     */
    public void protocolMsgCtrl(String message){
    
        try {
            out.writeUTF("message#" + message);
        } catch (IOException ex) {
            
        }
    }
    
    /**
     * Todos os eventos que acontecem no tabuleiro e precisam se enviados obedecem esse protocolo
     * @param nextLocation: Localização do próximo botão na tela do adversário
     * @param lastLocation: Localização do último botão na tela do adversário
     * @param nextIconState: Cor do próximo botão na tela do adversário
     * @param lastIconState: Cor do último botão na tela do adversário
     */
    public void protocolGameCtrl(Point nextLocation, Point lastLocation, Icon nextIconState, Icon lastIconState){
        
        try {
            System.out.println("enviei");
            out.writeUTF("game#" + nextLocation + "#" + lastLocation + "#" + nextIconState + "#" + lastIconState);
            out.flush();
    
        } catch (IOException ex) {
            System.out.println("Client connection error: " + ex.getMessage());
        }
        
    }
    
    /**
     * Protocolo de controle de turno
     */
    public  void protocolTurnCtrl(){
    
        try {
            out.writeUTF("turn#true");
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Protocolo e inicialização de comunicação
     */
    public static void startComunication(){
    
        try {
            out.writeUTF("me");
        } catch (IOException ex) {
            System.out.println("não foi possivel se conectar");
        }
    }

    /**
     * Protocolo de controle de desistencia
     * @param reason: motivo da desistencia
     */
    public  static void giveUpCtrl(String reason){
    
        try {
            out.writeUTF("giveup#" + reason);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Protocolo requisião de pedido de desistencia
     * @param answer: resposta da desisão
     */
    public static void restartRequest(String answer){
    
        try {
            out.writeUTF("restart#" + answer);
        } catch (IOException ex) {
            
        }
    }
    
    /**
     * Protocolo Resposta do pedido de desistencia
     * @param answer: resposta da desisão
     */
    public static void restartResponse(String answer){
    
        try {
            out.writeUTF("restartresponse#" + answer);
        } catch (IOException ex) {
            
        }
    }
    
    /**
     * Protocolo de indicação de vitoria 
     */
    public static void winnerCtrl(){
    
        try {
            out.writeUTF("winner");
        } catch (IOException e) {
        }
    }
    
    /**
     * Protocolo para aviso de Trilha
     */
    public static void trilhaAdvertise(){
    
        try {
            out.writeUTF("trilha");
        } catch (IOException e) {
        }
    }

    /**
     * Protocolo de aviso de peça tomada pelo adiversário
     */
    public static void pieceTakedAdvertise(){
    
        try {
            out.writeUTF("piecetaked");
        } catch (IOException e) {
        }
    }
}
