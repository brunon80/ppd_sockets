/*
 =======================================================
 * Autor: Bruno Rocha Rolim                            *
 * Curso: Engenharia de Computação                     *
 * 9º Semestre                                         *
 * Disciplina de Programação Paralela e Distribuida    *
 * Professor:  Cidcley T. de Souza                     *
 =======================================================
 * IDE de desenvolvimento: Netbeans v.8.1
 * Linguagem: Java Jdk v.8.23
 * Bibliotecas auxiliares: N/A
 * Todas as imagens são de autoria do desenvolvedor
 * Método de criação das imagens: Adobe Photoshop CC 2015, Adobe Illustrator CC 2015 e Spriter v15.0
 */


package sockets;
import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

/**
 * A Classe Client.java destina-se a criação da View  e possui métodos relacionados a mesma
 * @author Bruno R
 */
public class Client extends JFrame implements FocusListener{
    // <editor-fold defaultstate="collapsed" desc="Generated Code"> 
    static DataOutputStream out = null;
    static DataInputStream in = null; 		      
    static Socket soc = null; 
    static String host = "localhost";
    static String msg = "";
    static String msgToSend = "";
    static Scanner keyboard = new Scanner(System.in);
    static JTextArea chatArea;
    static JTextField msgField;
    static JButton tutorial ,restart ,giveUp, send, A0, A1, A2, A3, A4, A5, A6, A7, B0, B1, B2, B3, B4, B5, B6, B7, C0, C1, C2, C3, C4, C5, C6, C7, newTurn, back ;
    static JButton[] btnArray = new JButton[24] ;
    static JLabel[] btnMinbArray = new JLabel[9] ;
    static JLabel[] btnMinwArray = new JLabel[9] ;
    static String myId, protocolRecieved, dataProtocolRecivied[],player, me, origin;
    static ImageIcon backgroundSource = new ImageIcon("imgs/tabuleiro.png");
    static ImageIcon whiteIcon = new ImageIcon("imgs/w.png");
    static ImageIcon whiteMinIcon = new ImageIcon("imgs/wmin.png");
    static ImageIcon blackIcon = new ImageIcon("imgs/b.png");
    static ImageIcon blackMinIcon = new ImageIcon("imgs/bmin.png");    
    static ImageIcon redIcon = new ImageIcon("imgs/r.png");
    static ImageIcon noIcon = new ImageIcon("imgs/n.png");
    static ImageIcon noMinIcon = new ImageIcon("imgs/nmin.png");
    static ImageIcon myColorPieces = new ImageIcon("");
    static ImageIcon yourColorPieces = new ImageIcon("");
    static ImageIcon myMinPieces = new ImageIcon("imgs/bmin.png");
    static ImageIcon yourMinPieces = new ImageIcon("imgs/wmin.png");
    static ImageIcon sendIcon = new ImageIcon("imgs/sendIcon.png");
    static ImageIcon restartIcon = new ImageIcon("imgs/restartIcon.png");
    static ImageIcon giveUpIcon = new ImageIcon("imgs/giveUpIcon.png");
    static ImageIcon wrongIcon = new ImageIcon("imgs/wrongIcon.png"); 
    static ImageIcon newTurnIcon = new ImageIcon("imgs/newTurnIcon.png");  
    static ImageIcon tutorialIcon = new ImageIcon("imgs/tutorialIcon.png");    
    static ImageIcon nextIcon = new ImageIcon("");
    static ImageIcon lastIcon = new ImageIcon("");
    static JLabel background;
    static int myPieces = 9;
    static int myPiecesTaked = 0;
    static boolean nextPart = false;
    static boolean turn = false;
    static JButton lastButton = new JButton();
    static JButton tempbtn = new JButton();
    static String lastRecivied = "";
    static String nextRecivied = "";
    static boolean advertise = false;
    static Point lastLocation = null;
    static boolean firstConnection = true;
    static ComunicationCtrl connect = new ComunicationCtrl();
    static boolean responsed = false;
    static boolean trilha = false;
    // </editor-fold>

    /**
     * Esse método verifica a existencia de "Trilha" em um determinado botão.
     * @param btnSource: Origem do clique
     */
    public static void trilha(JButton btnSource){
    
        

        if (btnSource == A0){

            if (((A0.getIcon().toString().equals(myColorPieces.toString()) && A1.getIcon().toString().equals(myColorPieces.toString())) && A2.getIcon().toString().equals(myColorPieces.toString())) || ((A0.getIcon().toString().equals(myColorPieces.toString())) && A7.getIcon().toString().equals(myColorPieces.toString()) && A6.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A0");
                trilhaAction();
            }
        }
        if (btnSource == A1){

            if ((A0.getIcon().toString().equals(myColorPieces.toString()) && A1.getIcon().toString().equals(myColorPieces.toString()) && A2.getIcon().toString().equals(myColorPieces.toString())) || ((A1.getIcon().toString().equals(myColorPieces.toString())) && B1.getIcon().toString().equals(myColorPieces.toString()) && C1.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A1");
                trilhaAction();
            }
        }
        if (btnSource == A2){

            if ((A0.getIcon().toString().equals(myColorPieces.toString()) && A1.getIcon().toString().equals(myColorPieces.toString()) && A2.getIcon().toString().equals(myColorPieces.toString())) || ((A2.getIcon().toString().equals(myColorPieces.toString())) && A3.getIcon().toString().equals(myColorPieces.toString()) && A4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A2");
                trilhaAction();
            }
        }
        if (btnSource == A3){

            if ((A3.getIcon().toString().equals(myColorPieces.toString()) && B3.getIcon().toString().equals(myColorPieces.toString()) && C3.getIcon().toString().equals(myColorPieces.toString())) || ((A2.getIcon().toString().equals(myColorPieces.toString())) && A3.getIcon().toString().equals(myColorPieces.toString()) && A4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A3");
                trilhaAction();
            }
        }
        if (btnSource == A4){

            if ((A4.getIcon().toString().equals(myColorPieces.toString()) && A5.getIcon().toString().equals(myColorPieces.toString()) && A6.getIcon().toString().equals(myColorPieces.toString())) || ((A2.getIcon().toString().equals(myColorPieces.toString())) && A3.getIcon().toString().equals(myColorPieces.toString()) && A4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A4");
                trilhaAction();
            }
        }
        if (btnSource == A5){

            if ((A4.getIcon().toString().equals(myColorPieces.toString()) && A5.getIcon().toString().equals(myColorPieces.toString()) && A6.getIcon().toString().equals(myColorPieces.toString())) || ((A5.getIcon().toString().equals(myColorPieces.toString())) && B5.getIcon().toString().equals(myColorPieces.toString()) && C5.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A5");
                trilhaAction();
            }
        }
        if (btnSource == A6){

            if ((A4.getIcon().toString().equals(myColorPieces.toString()) && A5.getIcon().toString().equals(myColorPieces.toString()) && A6.getIcon().toString().equals(myColorPieces.toString())) || ((A0.getIcon().toString().equals(myColorPieces.toString())) && A7.getIcon().toString().equals(myColorPieces.toString()) && A6.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A6");
                trilhaAction();
            }
        }
        if (btnSource == A7){
            
            if ((A6.getIcon().toString().equals(myColorPieces.toString()) && A7.getIcon().toString().equals(myColorPieces.toString()) && A0.getIcon().toString().equals(myColorPieces.toString())) || ((A7.getIcon().toString().equals(myColorPieces.toString())) && B7.getIcon().toString().equals(myColorPieces.toString()) && C7.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no A7");
                trilhaAction();
            }
        }
        // Detecção de Trilha em B

        if (btnSource == B0){

            if ((B0.getIcon().toString().equals(myColorPieces.toString()) && B1.getIcon().toString().equals(myColorPieces.toString()) && B2.getIcon().toString().equals(myColorPieces.toString())) || ((B0.getIcon().toString().equals(myColorPieces.toString())) && B7.getIcon().toString().equals(myColorPieces.toString()) && B6.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B0");
                trilhaAction();
            }
        }
        if (btnSource == B1){

            if ((B0.getIcon().toString().equals(myColorPieces.toString()) && B1.getIcon().toString().equals(myColorPieces.toString()) && B2.getIcon().toString().equals(myColorPieces.toString())) || ((B1.getIcon().toString().equals(myColorPieces.toString())) && A1.getIcon().toString().equals(myColorPieces.toString()) && C1.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B1");
                trilhaAction();
            }
        }
        if (btnSource == B2){

            if ((B0.getIcon().toString().equals(myColorPieces.toString()) && B1.getIcon().toString().equals(myColorPieces.toString()) && B2.getIcon().toString().equals(myColorPieces.toString())) || ((B2.getIcon().toString().equals(myColorPieces.toString())) && B3.getIcon().toString().equals(myColorPieces.toString()) && B4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B2");
                trilhaAction();
            }
        }
        if (btnSource == B3){

            if ((A3.getIcon().toString().equals(myColorPieces.toString()) && B3.getIcon().toString().equals(myColorPieces.toString()) && C3.getIcon().toString().equals(myColorPieces.toString())) || ((B2.getIcon().toString().equals(myColorPieces.toString())) && B4.getIcon().toString().equals(myColorPieces.toString()) && B3.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B3");
                trilhaAction();
            }
        }
        if (btnSource == B4){

            if ((B4.getIcon().toString().equals(myColorPieces.toString()) && B5.getIcon().toString().equals(myColorPieces.toString()) && B6.getIcon().toString().equals(myColorPieces.toString())) || ((B2.getIcon().toString().equals(myColorPieces.toString())) && B3.getIcon().toString().equals(myColorPieces.toString()) && B4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B4");
                trilhaAction();
            }
        }
        if (btnSource == B5){

            if ((B4.getIcon().toString().equals(myColorPieces.toString()) && B5.getIcon().toString().equals(myColorPieces.toString()) && B6.getIcon().toString().equals(myColorPieces.toString())) || ((A5.getIcon().toString().equals(myColorPieces.toString())) && B5.getIcon().toString().equals(myColorPieces.toString()) && C5.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B5");
                trilhaAction();
            }
        }
        if (btnSource == B6){

            if ((B4.getIcon().toString().equals(myColorPieces.toString()) && B5.getIcon().toString().equals(myColorPieces.toString()) && B6.getIcon().toString().equals(myColorPieces.toString())) || ((B6.getIcon().toString().equals(myColorPieces.toString())) && B7.getIcon().toString().equals(myColorPieces.toString()) && B0.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B6");
                trilhaAction();
            }
        }
        if (btnSource == B7){

            if ((B6.getIcon().toString().equals(myColorPieces.toString()) && B7.getIcon().toString().equals(myColorPieces.toString()) && B0.getIcon().toString().equals(myColorPieces.toString())) || ((A7.getIcon().toString().equals(myColorPieces.toString())) && B7.getIcon().toString().equals(myColorPieces.toString()) && C7.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no B7");
                trilhaAction();
            }
        }

        //Detecção de trilha em C

        if (btnSource == C0){

            if ((C0.getIcon().toString().equals(myColorPieces.toString()) && C1.getIcon().toString().equals(myColorPieces.toString()) && C2.getIcon().toString().equals(myColorPieces.toString())) || ((C0.getIcon().toString().equals(myColorPieces.toString())) && C7.getIcon().toString().equals(myColorPieces.toString()) && C6.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C0");
                trilhaAction();
            }
        }
        if (btnSource == C1){

            if ((C0.getIcon().toString().equals(myColorPieces.toString()) && C1.getIcon().toString().equals(myColorPieces.toString()) && C2.getIcon().toString().equals(myColorPieces.toString())) || ((A1.getIcon().toString().equals(myColorPieces.toString())) && B1.getIcon().toString().equals(myColorPieces.toString()) && C1.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C1");
                trilhaAction();
            }
        }
        if (btnSource == C2){

            if ((C0.getIcon().toString().equals(myColorPieces.toString()) && C1.getIcon().toString().equals(myColorPieces.toString()) && C2.getIcon().toString().equals(myColorPieces.toString())) || ((C2.getIcon().toString().equals(myColorPieces.toString())) && C3.getIcon().toString().equals(myColorPieces.toString()) && C4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C2");
                trilhaAction();
            }
        }
        if (btnSource == C3){

            if ((A3.getIcon().toString().equals(myColorPieces.toString()) && B3.getIcon().toString().equals(myColorPieces.toString()) && C3.getIcon().toString().equals(myColorPieces.toString())) || ((C2.getIcon().toString().equals(myColorPieces.toString())) && C3.getIcon().toString().equals(myColorPieces.toString()) && C4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C3");
                trilhaAction();
            }
        }
        if (btnSource == C4){

            if ((C4.getIcon().toString().equals(myColorPieces.toString()) && C5.getIcon().toString().equals(myColorPieces.toString()) && C6.getIcon().toString().equals(myColorPieces.toString())) || ((C2.getIcon().toString().equals(myColorPieces.toString())) && C3.getIcon().toString().equals(myColorPieces.toString()) && C4.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C4");
                trilhaAction();
            }
        }
        if (btnSource == C5){

            if ((C4.getIcon().toString().equals(myColorPieces.toString()) && C5.getIcon().toString().equals(myColorPieces.toString()) && C6.getIcon().toString().equals(myColorPieces.toString())) || ((A5.getIcon().toString().equals(myColorPieces.toString())) && B5.getIcon().toString().equals(myColorPieces.toString()) && C5.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C5");
                trilhaAction();
            }
        }
        if (btnSource == C6){

            if ((C4.getIcon().toString().equals(myColorPieces.toString()) && C5.getIcon().toString().equals(myColorPieces.toString()) && C6.getIcon().toString().equals(myColorPieces.toString())) || ((C0.getIcon().toString().equals(myColorPieces.toString())) && C7.getIcon().toString().equals(myColorPieces.toString()) && C6.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C6");
                trilhaAction();
            }
        }
        if (btnSource == C7){
            
            if (((A7.getIcon().toString().equals(myColorPieces.toString())) && B7.getIcon().toString().equals(myColorPieces.toString()) && C7.getIcon().toString().equals(myColorPieces.toString())) || ((C7.getIcon().toString().equals(myColorPieces.toString())) && C0.getIcon().toString().equals(myColorPieces.toString()) && C6.getIcon().toString().equals(myColorPieces.toString()))){

                System.out.println("trilha no C7");
                trilhaAction();
            }
        }
        
    }
    
    /**
     * Método do que fazer quando a trilha acontece
     */
    public static void trilhaAction(){
    
        connect.trilhaAdvertise();
        chatArea.append("<Moderador> Trilha!!! Tire uma pedra do seu adversário!!!\n");
        chatArea.append("\n");
        trilha = true;
    }
    
    /**
     * Método para devolver uma peça pega por engano
     */
    public static void rollBack(){
        
        if(tempbtn.getIcon() == noIcon && lastButton.getIcon().toString().equals(myColorPieces.toString())){   
            putMinIcon();
           tempbtn.setIcon(yourColorPieces);
           connect.protocolGameCtrl(tempbtn.getLocation(), lastButton.getLocation(), tempbtn.getIcon(), lastButton.getIcon());
           
        }
       
   }
   
    /**
     * Método que ocorre quando se vence a partida
     */
    public static void winner(){
   
       if (myPiecesTaked == 7){
           connect.winnerCtrl();
           JOptionPane.showMessageDialog(null, "Parabéns, você venceu!");
           restart();
           
       }
   }
   
    /**
     * Esse método pinta de vermelho um botão quando entra em "foco"
     * @param fe: origem do foco
     */
    @Override
    public void focusGained(FocusEvent fe) {
    
        if (turn){
            
            if (nextPart == true){
                tempbtn = (JButton) fe.getSource();
                if(tempbtn.getIcon().toString().equals(myColorPieces.toString())){

                    tempbtn.setIcon(redIcon);


                }
            }
        }
        
        
    }

    /**
     * Esse método pinta de vermelho um botão quando entra em "foco"
     * @param fe: origem da perda do foco
     */
    @Override
    public void focusLost(FocusEvent fe) {
        
        if (nextPart == true){
        
            if(tempbtn.getIcon() == redIcon){
            
                tempbtn.setIcon(myColorPieces);
                
            }
        }
    }
   
    /**
     * Método de reinicialização da partida
     */
    public static void restart(){
        setAllMinIcon();
        chatArea.setText("");
        nextPart = false;
        for (int i = 0; i< btnArray.length; i++){

            btnArray[i].setIcon(noIcon);
            myPieces = 9;
            myPiecesTaked = 0;
            
        }
    }
    
    /**
     * Método que retira um icone pequeno
     */
    public static void takebMinIcon(){
        
        btnMinbArray[myPieces -1].setIcon(noMinIcon);
    }

    /**
     * Método que põe um icone pequeno
     */
    public static void putMinIcon(){
        myPiecesTaked --;
        btnMinwArray[myPiecesTaked].setIcon(noMinIcon);
        
    }
    
    /**
     * Método que põe todos os icones pequenos
     */
    public static void setAllMinIcon(){
    
        for (int i = 0; i< btnMinbArray.length; i++){
        
            btnMinbArray[i].setIcon(myMinPieces);
        }
        for (int i = 0; i< btnMinwArray.length; i++){
        
            btnMinwArray[i].setIcon(noMinIcon);
        }
    }
    
    /**
     * Método que põe um icone pequeno do adversário
     */
    public static void putwMinIcon(){
    
        btnMinwArray[myPiecesTaked].setIcon(yourMinPieces);
    }
  
    /**
     * Inicialização da comunicação e conexão com o servidor
     * @param args
     */
    public static void main(String[] args) {
        new HelpWindow();
        lastButton.setIcon(new ImageIcon(""));
        ListenForSocket t1 = new ListenForSocket();
        t1.start();        
        new Client();
        try {
            connect.startComunication();
        } catch (Exception e) {
            System.out.println("não conectei");
        }
        
        		
    }
    
    /**
     * Mega Método de gera essa bela interface
     */
    public Client(){
        this.setSize(990,678);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("Trilha & $ockets!");
        this.setResizable(false);
        this.getContentPane().setBackground(Color.CYAN);
        this.setIconImage(myMinPieces.getImage());
        
        ListenForButton lForButton = new ListenForButton();
        ListenForToolBarButton lForToolBar = new ListenForToolBarButton();
        
        JPanel mainGamePanel = new JPanel();
        mainGamePanel.setBackground(new Color(233, 232, 230));
        mainGamePanel.setLayout(null);
        //mainGamePanel.setBounds(0, 0, 400, 400);
        
        JLabel backGround = new JLabel();
        backGround.setIcon(backgroundSource);
        backGround.setBounds(0, 0, 600, 600);
        
        
        // Primeiro Quadrado do tabuleiro
        A0 = new JButton();
        A0.setOpaque(false);
        A0.setContentAreaFilled(false);
        A0.setBorderPainted(false);
        A0.setIcon(noIcon);
        A0.setBounds( 15, 15, 64, 64);
        A0.addActionListener(lForButton);
        A0.addFocusListener(this);
        btnArray[0] = A0;
        
        A1 = new JButton();
        A1.setOpaque(false);
        A1.setContentAreaFilled(false);
        A1.setBorderPainted(false);
        A1.setIcon(noIcon);
        A1.setBounds( 270, 15, 64, 64);
        A1.addActionListener(lForButton);
        A1.addFocusListener(this);
        btnArray[1] = A1;
        
        A2 = new JButton();
        A2.setOpaque(false);
        A2.setContentAreaFilled(false);
        A2.setBorderPainted(false);
        A2.setIcon(noIcon);
        A2.setBounds( 520, 15, 64, 64);
        A2.addActionListener(lForButton);
        A2.addFocusListener(this);
        btnArray[2] = A2;
        
        A3 = new JButton();
        A3.setOpaque(false);
        A3.setContentAreaFilled(false);
        A3.setBorderPainted(false);
        A3.setIcon(noIcon);
        A3.setBounds( 520, 270, 64, 64);
        A3.addActionListener(lForButton);
        A3.addFocusListener(this);
        btnArray[3] = A3;
        
        A4 = new JButton();
        A4.setOpaque(false);
        A4.setContentAreaFilled(false);
        A4.setBorderPainted(false);
        A4.setIcon(noIcon);
        A4.setBounds( 520, 520, 64, 64);
        A4.addActionListener(lForButton);
        A4.addFocusListener(this);
        btnArray[4] = A4;
        
        A5 = new JButton();
        A5.setOpaque(false);
        A5.setContentAreaFilled(false);
        A5.setBorderPainted(false);
        A5.setIcon(noIcon);
        A5.setBounds( 270, 520, 64, 64);
        A5.addActionListener(lForButton);
        A5.addFocusListener(this);
        btnArray[5] = A5;
        
        A6 = new JButton();
        A6.setOpaque(false);
        A6.setContentAreaFilled(false);
        A6.setBorderPainted(false);
        A6.setIcon(noIcon);
        A6.setBounds( 15, 520, 64, 64);
        A6.addActionListener(lForButton);
        A6.addFocusListener(this);
        btnArray[6] = A6;
        
        A7 = new JButton();
        A7.setOpaque(false);
        A7.setContentAreaFilled(false);
        A7.setBorderPainted(false);
        A7.setIcon(noIcon);
        A7.setBounds( 15, 270, 64, 64);
        A7.addActionListener(lForButton);
        A7.addFocusListener(this);
        btnArray[7] = A7;
        
        // Segundo Quadrado do tabuleiro
        
        B0 = new JButton();
        B0.setOpaque(false);
        B0.setContentAreaFilled(false);
        B0.setBorderPainted(false);
        B0.setIcon(noIcon);
        B0.setBounds( 100, 100, 64, 64);
        B0.addActionListener(lForButton);
        B0.addFocusListener(this);
        btnArray[8] = B0;
        
        B1 = new JButton();
        B1.setOpaque(false);
        B1.setContentAreaFilled(false);
        B1.setBorderPainted(false);
        B1.setIcon(noIcon);
        B1.setBounds( 270, 100, 64, 64);
        B1.addActionListener(lForButton);
        B1.addFocusListener(this);
        btnArray[9] = B1;
        
        B2 = new JButton();
        B2.setOpaque(false);
        B2.setContentAreaFilled(false);
        B2.setBorderPainted(false);
        B2.setIcon(noIcon);
        B2.setBounds( 440, 100, 64, 64);
        B2.addActionListener(lForButton);
        B2.addFocusListener(this);
        btnArray[10] = B2;
        
        B3 = new JButton();
        B3.setOpaque(false);
        B3.setContentAreaFilled(false);
        B3.setBorderPainted(false);
        B3.setIcon(noIcon);
        B3.setBounds( 440, 270, 64, 64);
        B3.addActionListener(lForButton);
        B3.addFocusListener(this);
        btnArray[11] = B3;
        
        B4 = new JButton();
        B4.setOpaque(false);
        B4.setContentAreaFilled(false);
        B4.setBorderPainted(false);
        B4.setIcon(noIcon);
        B4.setBounds( 440, 440, 64, 64);
        B4.addActionListener(lForButton);
        B4.addFocusListener(this);
        btnArray[12] = B4;
        
        B5 = new JButton();
        B5.setOpaque(false);
        B5.setContentAreaFilled(false);
        B5.setBorderPainted(false);
        B5.setIcon(noIcon);
        B5.setBounds( 270, 440, 64, 64);
        B5.addActionListener(lForButton);
        B5.addFocusListener(this);
        btnArray[13] = B5;
        
        B6 = new JButton();
        B6.setOpaque(false);
        B6.setContentAreaFilled(false);
        B6.setBorderPainted(false);
        B6.setIcon(noIcon);
        B6.setBounds( 100, 440, 64, 64);
        B6.addActionListener(lForButton);
        B6.addFocusListener(this);
        btnArray[14] = B6;
        
        B7 = new JButton();
        B7.setOpaque(false);
        B7.setContentAreaFilled(false);
        B7.setBorderPainted(false);
        B7.setIcon(noIcon);
        B7.setBounds( 100, 270, 64, 64);
        B7.addActionListener(lForButton);
        B7.addFocusListener(this);
        btnArray[15] = B7;
        
        // Terceiro Quadrado do tabuleiro
        
        C0 = new JButton();
        C0.setOpaque(false);
        C0.setContentAreaFilled(false);
        C0.setBorderPainted(false);
        C0.setIcon(noIcon);
        C0.setBounds( 180, 180, 64, 64);
        C0.addActionListener(lForButton);
        C0.addFocusListener(this);
        btnArray[16] = C0;
        
        C1 = new JButton();
        C1.setOpaque(false);
        C1.setContentAreaFilled(false);
        C1.setBorderPainted(false);
        C1.setIcon(noIcon);
        C1.setBounds( 270, 180, 64, 64);
        C1.addActionListener(lForButton);
        C1.addFocusListener(this);
        btnArray[17] = C1;
        
        C2 = new JButton();
        C2.setOpaque(false);
        C2.setContentAreaFilled(false);
        C2.setBorderPainted(false);
        C2.setIcon(noIcon);
        C2.setBounds( 358, 180, 64, 64);
        C2.addActionListener(lForButton);
        C2.addFocusListener(this);
        btnArray[18] = C2;
        
        C3 = new JButton();
        C3.setOpaque(false);
        C3.setContentAreaFilled(false);
        C3.setBorderPainted(false);
        C3.setIcon(noIcon);
        C3.setBounds( 358, 270, 64, 64);
        C3.addActionListener(lForButton);
        C3.addFocusListener(this);
        btnArray[19] = C3;
        
        C4 = new JButton();
        C4.setOpaque(false);
        C4.setContentAreaFilled(false);
        C4.setBorderPainted(false);
        C4.setIcon(noIcon);
        C4.setBounds( 358, 358, 64, 64);
        C4.addActionListener(lForButton);
        C4.addFocusListener(this);
        btnArray[20] = C4;
        
        C5 = new JButton();
        C5.setOpaque(false);
        C5.setContentAreaFilled(false);
        C5.setBorderPainted(false);
        C5.setIcon(noIcon);
        C5.setBounds( 270, 358, 64, 64);
        C5.addActionListener(lForButton); 
        C5.addFocusListener(this);
        btnArray[21] = C5;
        
        C6 = new JButton();
        C6.setOpaque(false);
        C6.setContentAreaFilled(false);
        C6.setBorderPainted(false);
        C6.setIcon(noIcon);
        C6.setBounds( 180, 358, 64, 64);
        C6.addActionListener(lForButton);
        C6.addFocusListener(this);
        btnArray[22] = C6;
        
        C7 = new JButton();
        C7.setOpaque(false);
        C7.setContentAreaFilled(false);
        C7.setBorderPainted(false);
        C7.setIcon(noIcon);
        C7.setBounds( 180, 270, 64, 64);
        
        C7.addActionListener(lForButton);
        C7.addFocusListener(this);
        btnArray[23] = C7;
        
        // Botões A0
        mainGamePanel.add(A0);
        mainGamePanel.add(A1);
        mainGamePanel.add(A2);
        mainGamePanel.add(A3);
        mainGamePanel.add(A4);
        mainGamePanel.add(A5);
        mainGamePanel.add(A6);
        mainGamePanel.add(A7);
        
        //Botões B0
        mainGamePanel.add(B0);
        mainGamePanel.add(B1);
        mainGamePanel.add(B2);
        mainGamePanel.add(B3);
        mainGamePanel.add(B4);
        mainGamePanel.add(B5);
        mainGamePanel.add(B6);
        mainGamePanel.add(B7);
        
        //Botões C0
        mainGamePanel.add(C0);
        mainGamePanel.add(C1);
        mainGamePanel.add(C2);
        mainGamePanel.add(C3);
        mainGamePanel.add(C4);
        mainGamePanel.add(C5);
        mainGamePanel.add(C6);
        mainGamePanel.add(C7);
        
        
        
        mainGamePanel.add(backGround);        
        
        JPanel mainChatPanel = new JPanel();
        mainChatPanel.setSize(400, 700);
        //mainChatPanel.setBackground(new Color(252, 230, 191));
        mainChatPanel.setLayout(new BorderLayout());
        
        JPanel mainPanel = new JPanel();
        //mainPanel.setBackground(new Color(252, 230, 191));
        mainPanel.setLayout(new BorderLayout());
        
        chatArea = new JTextArea();  
        Border border = BorderFactory.createLineBorder(Color.WHITE);
        chatArea.setBorder(BorderFactory.createCompoundBorder(border,BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        chatArea.setLineWrap(true);
        chatArea.setEditable(false);
        chatArea.setWrapStyleWord(true);
        chatArea.setMargin(new Insets(10,10,10,10));
        DefaultCaret caret = (DefaultCaret)chatArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        chatArea.append("<Moderador> Bem vindo ao Trilha e $ockets!\n");
        chatArea.append("\n");
        JScrollPane scroll = new JScrollPane(chatArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        mainChatPanel.add(scroll, BorderLayout.CENTER);
        
        JPanel bottonPanel = new JPanel();
        bottonPanel.setBackground(new Color(233, 232, 230));
        bottonPanel.setLayout(new FlowLayout());
        
        msgField = new JTextField("", 21);
        msgField.setPreferredSize(new Dimension(25,32));
        msgField.setMargin(new Insets(0, 10, 0, 0));
        msgField.setBorder(BorderFactory.createCompoundBorder(border,BorderFactory.createEmptyBorder(0, 10, 0, 0)));
        msgField.requestFocus(true);
        ListenForButton lForKeys = new ListenForButton();
        msgField.addKeyListener(lForKeys);
        
        send = new JButton("");
        send.setIcon(sendIcon);
        send.setOpaque(false);
        send.setContentAreaFilled(false);
        send.setMargin(new Insets(0, 0, 0, 0));
        send.setBorderPainted(false);
        send.addActionListener(lForToolBar);
        
        bottonPanel.add(msgField);
        bottonPanel.add(send);
        
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        JToolBar tools = new JToolBar();
        tools.setBorder(null);
        tools.setFloatable(false);
        tools.setBackground(new Color(0, 136, 122));
        mainPanel.add(tools, BorderLayout.PAGE_START);
        restart = new JButton("");
        restart.setIcon(restartIcon);
        restart.setOpaque(false);
        restart.setContentAreaFilled(false);
        restart.setBorderPainted(false);
        restart.addActionListener(lForToolBar);
        tools.add(restart);
        giveUp = new JButton("");
        giveUp.setIcon(giveUpIcon);
        giveUp.setOpaque(false);
        giveUp.setContentAreaFilled(false);
        giveUp.setBorderPainted(false);
        giveUp.addActionListener(lForToolBar);
        tools.add(giveUp); 
        back = new JButton("");
        back.setIcon(wrongIcon);
        back.setOpaque(false);
        back.setContentAreaFilled(false);
        back.setBorderPainted(false);
        back.addActionListener(lForToolBar);
        tools.add(back); 
        
        newTurn = new JButton("");
        newTurn.setIcon(newTurnIcon);
        newTurn.setOpaque(false);
        newTurn.setContentAreaFilled(false);
        newTurn.setBorderPainted(false);
        newTurn.addActionListener(lForToolBar);
        tools.add(newTurn); 
        
        tutorial = new JButton("");
        tutorial.setIcon(tutorialIcon);
        tutorial.setOpaque(false);
        tutorial.setContentAreaFilled(false);
        tutorial.setBorderPainted(false);
        tutorial.addActionListener(lForToolBar);
        tools.add(tutorial); 
        
        
        JPanel myPiecesIcon = new JPanel();
        myPiecesIcon.setBackground(new Color(233, 232, 230));
        myPiecesIcon.setLayout(new FlowLayout());
        
        
        JLabel btn1 = new JLabel();
        btn1.setOpaque(false);
        btn1.setSize(1, 1);
        btn1.setIcon(myMinPieces);
        btnMinbArray[0] = btn1;
        
        JLabel btn2 = new JLabel();
        btn2.setOpaque(false);
        btn2.setIcon(myMinPieces);
        btnMinbArray[1] = btn2;
        
        JLabel btn3 = new JLabel();
        btn3.setOpaque(false);
        btn3.setIcon(myMinPieces);
        btnMinbArray[2] = btn3;
        
        JLabel btn4 = new JLabel();
        btn4.setOpaque(false);
        btn4.setIcon(myMinPieces);
        btnMinbArray[3] = btn4;
        
        JLabel btn5 = new JLabel();
        btn5.setOpaque(false);
        btn5.setIcon(myMinPieces);
        btnMinbArray[4] = btn5;
        
        JLabel btn6 = new JLabel();
        btn6.setOpaque(false);
        btn6.setIcon(myMinPieces);
        btnMinbArray[5] = btn6;
        
        JLabel btn7 = new JLabel();
        btn7.setOpaque(false);
        btn7.setIcon(myMinPieces);
        btnMinbArray[6] = btn7;
        
        JLabel btn8 = new JLabel();
        btn8.setOpaque(false);
        btn8.setIcon(myMinPieces);
        btnMinbArray[7] = btn8;
        
        JLabel btn9 = new JLabel();
        btn9.setOpaque(false);
        btn9.setIcon(myMinPieces);
        btnMinbArray[8] = btn9;
        
        
        
        myPiecesIcon.add(btn1);
        myPiecesIcon.add(btn2);
        myPiecesIcon.add(btn3);
        myPiecesIcon.add(btn4);
        myPiecesIcon.add(btn5);
        myPiecesIcon.add(btn6);
        myPiecesIcon.add(btn7);
        myPiecesIcon.add(btn8);
        myPiecesIcon.add(btn9);
        
        JPanel youPiecesIcon = new JPanel();
        youPiecesIcon.setBackground(new Color(233, 232, 230));
        youPiecesIcon.setLayout(new FlowLayout());
        
        
        JLabel btn10 = new JLabel();
        btn10.setOpaque(false);
        btn10.setSize(1, 1);
        btn10.setIcon(noMinIcon);
        btnMinwArray[0] = btn10;
        
        JLabel btn11 = new JLabel();
        btn11.setOpaque(false);
        btn11.setIcon(noMinIcon);
        btnMinwArray[1] = btn11;
        
        JLabel btn12 = new JLabel();
        btn12.setOpaque(false);
        btn12.setIcon(noMinIcon);
        btnMinwArray[2] = btn12;
        
        JLabel btn13 = new JLabel();
        btn13.setOpaque(false);
        btn13.setIcon(noMinIcon);
        btnMinwArray[3] = btn13;
        
        JLabel btn14 = new JLabel();
        btn14.setOpaque(false);
        btn14.setIcon(noMinIcon);
        btnMinwArray[4] = btn14;
        
        JLabel btn15 = new JLabel();
        btn15.setOpaque(false);
        btn15.setIcon(noMinIcon);
        btnMinwArray[5] = btn15;
        
        JLabel btn16 = new JLabel();
        btn16.setOpaque(false);
        btn16.setIcon(noMinIcon);
        btnMinwArray[6] = btn16;
        
        JLabel btn17 = new JLabel();
        btn17.setOpaque(false);
        btn17.setIcon(noMinIcon);
        btnMinwArray[7] = btn17;
        
        JLabel btn18 = new JLabel();
        btn18.setOpaque(false);
        btn18.setIcon(noMinIcon);
        btnMinwArray[8] = btn18;
        
        
        
        youPiecesIcon.add(btn10);
        youPiecesIcon.add(btn11);
        youPiecesIcon.add(btn12);
        youPiecesIcon.add(btn13);
        youPiecesIcon.add(btn14);
        youPiecesIcon.add(btn15);
        youPiecesIcon.add(btn16);
        youPiecesIcon.add(btn17);
        youPiecesIcon.add(btn18);
        
        JPanel container = new JPanel();
        container.setBackground(new Color(233, 232, 230));
        container.setBorder(BorderFactory.createTitledBorder("Olhe as peças!"));
        BoxLayout layout = new BoxLayout(container, BoxLayout.Y_AXIS);
        //container.setSize(10, 10);
        container.setLayout(layout);
        container.add(myPiecesIcon);
        container.add(youPiecesIcon);
        
        
        mainChatPanel.add(container, BorderLayout.NORTH);
        mainChatPanel.add(bottonPanel, BorderLayout.SOUTH);
        mainPanel.add(mainGamePanel, BorderLayout.CENTER);
        mainPanel.add(mainChatPanel, BorderLayout.EAST);
        ListenForButton lForWindow = new ListenForButton();
        this.addWindowListener(lForWindow);
        this.add(mainPanel);
        this.setVisible(true);
        
        
    }
    
}
