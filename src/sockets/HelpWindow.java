/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Classe que possui o tutorial
 * @author Bruno R
 */
public class HelpWindow extends JFrame implements ActionListener{
    String textOne = "<html><font color='white'><body bgcolor=\"red\" >Colocando as peças: <br> <br> Esta é a fase inicial do jogo<br> onde cada jogador coloca uma <br>" +
    "peça de cada vez alternando entre<br> jogadores, caso um dos jogadores <br>forme uma" +
    " linha horizontal ou vertical<br> com três peças (um moinho), ele <br> terá o direito de " +
    "remover uma peça<br> de seu adversário do tabuleiro.<br><br><br><br><br><br><br></body></font></html>";
    String textTwo = "<html><font color='white'><body bgcolor=\"red\" >Movendo as peças: <br> <br> Esta fase se inicia quando ambos<br> os jogadores colocarem " +
    "suas <br>nove peças em jogo.<br> Consiste em mover suas<br> peças ao longo de uma das <br>" +
    "linhas do tabuleiro para uma<br> outra casa adjacente. Caso um<br> dos jogadores" +
    "tenha somente 3 peças<br> em jogo, ele pode \"voar\" com <br>suas peças, podendo " +
    "mover para<br> qualquer casa que não esteja <br>ocupada por uma peça <br>do adversário.</body></font></html>";
    String textThree = "<html><font color='white'><body bgcolor=\"red\" >Fim da Partida: <br> <br> O jogo só termina quando  3<br> situações" + "são alcançadas: <br><br>\n" +
    "Se um jogador reduzir as peças <br> de seu adversário para 2.<br><br>" + " Se um jogador deixar seu adversário <br>sem  nenhuma jogada válida. <br> Caso seu\n" +
    "adversário tenha somente<br> 3 peças em jogo, ele <br>não poderá ser \"trancado\"." + "<br><br> Se ambos os jogadores<br> ficarem somente com 3 <br> peças, o jogo acaba em <br> 10 rodadas." + "</body></font></html>";
    static ImageIcon firstGif = new ImageIcon("imgs/first.gif");
    static ImageIcon secondtGif = new ImageIcon("imgs/second.gif");
    static ImageIcon thirdtGif = new ImageIcon("imgs/third.gif");
    static ImageIcon nextIcon = new ImageIcon("imgs/nextIcon.png");    
    JLabel labelImg = new JLabel(firstGif);
    JLabel labelInfo = new JLabel();
    JButton nextBtn;
    int nextTip = 0;
    
    /**
     * janela de ajuda
     */
    public HelpWindow(){
    
        this.setSize(550,350);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setBackground(new Color(0x00887a));
        this.setTitle("Trilha & $ockets Tutorial");
        this.setResizable(false);
        this.setAlwaysOnTop(true);
        
        nextBtn = new JButton("Próxima");
        nextBtn.addActionListener(this);
        
        JPanel mainHelpPanel = new JPanel();        
        mainHelpPanel.setLayout(new BorderLayout());
        mainHelpPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainHelpPanel.setOpaque(true);
        mainHelpPanel.setBackground(new Color(0x00887a));
        
        
        
        labelImg.setBorder(new EmptyBorder(0, 0, 0, 0));
        labelImg.setOpaque(true);
        labelImg.setBackground(new Color(0x00887a));
        
        labelInfo.setBorder(new EmptyBorder(0, 0, 0, 0));
        labelInfo.setFont(new Font("Verdana", Font.PLAIN, 12));
        labelInfo.setOpaque(true);
        labelInfo.setBackground(new Color(0x00887a));
        labelInfo.setText(textOne);
        labelInfo.setSize(200, 350);
        labelInfo.setBorder(new EmptyBorder(5, 5, 5, 5));        
                
        mainHelpPanel.add(labelImg, BorderLayout.CENTER); 
        mainHelpPanel.add(labelInfo, BorderLayout.EAST);
        mainHelpPanel.add(nextBtn, BorderLayout.SOUTH );
        
        this.add(mainHelpPanel);
        
        this.setVisible(rootPaneCheckingEnabled);
        
    }

    /**
     * Passar para prpxima dica
     * @param ae: origem do clique
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if (ae.getSource() == nextBtn && nextTip == 0){
            
            labelInfo.setText(textTwo);
            labelImg.setIcon(secondtGif);
            nextTip ++;            
            
        }
        else if (ae.getSource() == nextBtn && nextTip == 1){
        
            labelInfo.setText(textThree);
            labelImg.setIcon(thirdtGif);
            nextBtn.setName("Fim :D");
            
        }
    }
}
